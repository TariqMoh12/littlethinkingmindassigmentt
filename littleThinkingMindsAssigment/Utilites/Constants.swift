//
//  Constants.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/21/21.
//

import Foundation
import UIKit
class Constants{
    
    struct Colors {
        static let whiteColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0)
        static let blackColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0)
        static let LightGrayColor = UIColor(red: 224/255, green: 224/255, blue: 224/255, alpha: 1.0)
        static let mainBlue = UIColor(red: 102/255, green: 215/255, blue: 255/255, alpha: 1.0)
        static let DarkGrayColor = UIColor(red: 192/255, green: 192/255, blue: 192/255, alpha: 1.0)
    }
    
    struct Links {
        static let topFree = "https://rss.itunes.apple.com/api/v1/jo/ios-apps/top-free/all/"
        static let topPaid = "https://rss.itunes.apple.com/api/v1/jo/ios-apps/top-paid/all/"
       
        
    }
    
}



