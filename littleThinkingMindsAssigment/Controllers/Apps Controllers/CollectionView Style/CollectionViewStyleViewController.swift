//
//  CollectionViewStyleViewController.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import UIKit

class CollectionViewStyleViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    private lazy var dataSource = FreeAppsDataSourceCollection(self,collectionView)
    private lazy var viewModel : FreeAppsViewModel = {
        let viewModel = FreeAppsViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    override func viewWillAppear(_ animated: Bool) {
           
        if !Connectivity.isConnectedToInternet {
            self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
            self.collectionView.reloadData()

        }else{
            self.setup()
            self.dataSource.viewModel = self.viewModel
            collectionView.backgroundColor = .white
            self.collectionView.reloadData()
        }

        

    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
  

}
private extension CollectionViewStyleViewController {
    
    func setup(){
        self.viewModel.fetchData(numberOfApps: 15) { (bool) in
            self.collectionView.reloadData()
        }
        DispatchQueue.main.async {
            self.dataSource.data?.addAndNotify(observer: self) { [weak self] in
                guard let self = self else {return}
                self.collectionView.reloadData()
            }

        }
    }
    
}

    




// MARK: - UICollectionViewDelegate protocol


