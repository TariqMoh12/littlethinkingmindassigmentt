//
//  FreeAppsCollectionViewCell.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import UIKit
import RealmSwift
class FreeAppsCollectionViewCell: UICollectionViewCell {
    let realm = try! Realm()

    @IBOutlet weak var favoriteImageView: UIImageView!
    @IBOutlet weak var appImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
      
    }
    
//    static var nib:UINib {
//                   return UINib(nibName: identifier, bundle: nil)
//               }
//               static var identifier: String {
//                   return String(describing: self)
//               }
    
   
}

