//
//  AppsTableViewCell.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import UIKit
import MOLH
import RealmSwift
class AppsTableViewCell: UITableViewCell {

    var navigationController : UINavigationController?
    let realm = try! Realm()

    @IBOutlet weak var appImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var favoriteImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        favoriteImageView.isUserInteractionEnabled = true
        favoriteImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(favoriteTapped)))
        
        if MOLHLanguage.isArabic(){
            self.appNameLabel.textAlignment = .right
        }else{
            self.appNameLabel.textAlignment = .left

        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //    var results: [Result]?

    var model:Result?{
        didSet{
            guard let model = model else {return}

            appNameLabel.text = model.artistName
            guard let imageURL = URL(string: model.artworkUrl100 ?? "") else { return}
            appImageView.load(url: imageURL)
            favoriteImageView.image = UIImage(named: "favorite_ic")

            }
            


          
        }
    
    @objc func favoriteTapped(){

        
        let folders =  realm.objects(Folders.self)
        if folders.isEmpty {
            let alert = UIAlertController(title: "Alert", message: "Please creat folder", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
            UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
        }else{
            var allFolders = Array<Folders>()
            for folder in folders {
                allFolders.append(folder)

            }
            let storyborad = UIStoryboard(name: "Favorites", bundle: nil)
            let vc = storyborad.instantiateViewController(withIdentifier: "FavoriteAppsVC") as! FavoriteAppsVC
            vc.allFolders = allFolders
            vc.model = model
            UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)

        }
        
            print("must set as favorite in realm")
//        }else{
//
//        }
    }
    }

class Folders:Object{
    @objc dynamic var folderID:String = ""
    @objc dynamic var folderName:String? = ""
    @objc dynamic var folderDiscreptions:String? = ""
    let appListIds = List<Int>()
    let appListName = List<String>()
    let appListURL = List<String>()
    let appListImages = List<String>()

        override class func primaryKey() -> String? {
            return "folderID"
        }
    

}

