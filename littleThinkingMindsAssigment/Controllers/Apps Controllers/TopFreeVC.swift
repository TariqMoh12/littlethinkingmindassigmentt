//
//  TopFreeVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import UIKit
import PaginatedTableView
class TopFreeVC: UIViewController {
    
    @IBOutlet weak var contannerView: UIView!
    @IBOutlet weak var segmetnSelected: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        segmetnSelected.setTitle(NSLocalizedString("TableView_Style", comment: ""), forSegmentAt: 0)
        segmetnSelected.setTitle(NSLocalizedString("CollectionView_Style", comment: ""), forSegmentAt: 1)

    }
 
    @IBAction func sgmentClicked(_ sender: Any) {
        if segmetnSelected.selectedSegmentIndex == 0 {
            let storyboard = UIStoryboard(name: "Apps", bundle: nil)
                let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "TableViewStyleViewController") as! TableViewStyleViewController

            //add as a childviewcontroller
            addChild(controller)
                

             // Add the child's View as a subview
             self.contannerView.addSubview(controller.view)
                controller.view.frame = self.contannerView.bounds
             controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

             // tell the childviewcontroller it's contained in it's parent
            controller.didMove(toParent: self)
            
            }else{
                let storyboard = UIStoryboard(name: "Apps", bundle: nil)
                let controller: UIViewController = storyboard.instantiateViewController(withIdentifier: "CollectionViewStyleViewController") as! CollectionViewStyleViewController

                 //add as a childviewcontroller
                 addChild(controller)

                  // Add the child's View as a subview
                  self.contannerView.addSubview(controller.view)
                  controller.view.frame = self.contannerView.bounds
                  controller.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

                  // tell the childviewcontroller it's contained in it's parent
                 controller.didMove(toParent: self)
            }
        
    }
}
