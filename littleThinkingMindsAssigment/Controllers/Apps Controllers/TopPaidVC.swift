//
//  TopPaidVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import UIKit
import PaginatedTableView
class TopPaidVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    private lazy var dataSource = PaidAppsDataSource(self,tableView)
    private lazy var viewModel : PaidAppsViewModel = {
        let viewModel = PaidAppsViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !Connectivity.isConnectedToInternet {
            self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
            self.tableView.reloadData()

        }else{
            setup()
            
            self.dataSource.viewModel = viewModel
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
       

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        


    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
private extension TopPaidVC {
    
    func setup(){
        self.dataSource.data?.addAndNotify(observer: self) { [weak self] in
            guard let self = self else {return}
            self.tableView.reloadData()
        }
        self.viewModel.fetchData(numberOfApps: 20) { (bool) in
            self.tableView.reloadData()

        }
    }
    
}
