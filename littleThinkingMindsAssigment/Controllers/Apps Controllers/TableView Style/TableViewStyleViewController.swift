//
//  TableViewStyleViewController.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import UIKit
import PaginatedTableView
class TableViewStyleViewController: UIViewController {

  
    @IBOutlet weak var tableView: UITableView!
    private lazy var dataSource = FreeAppsDataSourceTableView(self,tableView)
    private lazy var viewModel : FreeAppsViewModel = {
        let viewModel = FreeAppsViewModel(dataSource: dataSource)
        return viewModel
    }()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !Connectivity.isConnectedToInternet {
            self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
            self.tableView.reloadData()

        }else{
            
            setup()
            
            self.dataSource.viewModel = viewModel
            self.tableView.reloadData()
        }
        

    }
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

 
}
private extension TableViewStyleViewController {
    
    func setup(){
        self.dataSource.data?.addAndNotify(observer: self) { [weak self] in
            guard let self = self else {return}
            self.tableView.reloadData()
        }
        
        self.viewModel.fetchData(numberOfApps: 10) { (bool) in
            self.tableView.reloadData()
        }
    }
    
}
