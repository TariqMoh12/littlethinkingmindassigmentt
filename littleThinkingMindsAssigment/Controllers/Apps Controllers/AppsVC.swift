//
//  AppsVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/21/21.
//

import UIKit
import PagingKit
import SideMenu

class AppsVC: UIViewController {

    
    var menuViewController : PagingMenuViewController!
    var  contentViewController : PagingContentViewController!
    var titles = [NSLocalizedString("top_free", comment: ""), NSLocalizedString("top_paid", comment: "")]
    
    lazy var TopFreeController : UIViewController = {
        let storyborad = UIStoryboard(name: "Apps", bundle: nil)
        let vc = storyborad.instantiateViewController(withIdentifier: "TopFreeVC")
        return vc
    }()
    
    lazy var TopPaidController : UIViewController = {
        let storyborad = UIStoryboard(name: "Apps", bundle: nil)
        let vc = storyborad.instantiateViewController(withIdentifier: "TopPaidVC")
        return vc
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        contentViewController.scrollView.isScrollEnabled = true
        contentViewController.view.backgroundColor = .clear
        setUpMenuViewController()

        tabBarController?.tabBar.barTintColor = Constants.Colors.mainBlue
        tabBarController?.tabBar.tintColor = Constants.Colors.whiteColor
        // Do any additional setup after loading the view.
    }
    

    
    @IBAction func createFolderPressed(_ sender: Any) {
   
            let storyborad = UIStoryboard(name: "Main", bundle: nil)
        
            let vc = storyborad.instantiateViewController(withIdentifier: "CreateFolderVC")
        present(vc, animated: true, completion: nil)
       
    }
    
    
    func setUpMenuViewController(){

        menuViewController.register(nib: UINib(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        menuViewController.registerFocusView(nib: UINib(nibName: "FocusView", bundle: nil))

        contentViewController.scroll(to: 0, animated: true) { (success) in
            self.contentViewController.reloadData()
            self.contentViewController.reloadData()

            self.menuViewController.reloadData()
        }
   
              

        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController{
            menuViewController = vc
            menuViewController.dataSource = self
            menuViewController.delegate = self

        }else if let vc = segue.destination as? PagingContentViewController{
            contentViewController = vc
                       contentViewController.dataSource = self
                       contentViewController.delegate = self

        }
    }
    
}
extension AppsVC: PagingMenuViewControllerDataSource {
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return titles.count
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return (UIScreen.main.bounds.width / CGFloat(titles.count))
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: index) as! MenuCell
       // let cell = viewController.dequeueReusableCell(withReuseIdentifier: "TapView", for: index) as! TabView
        cell.titleLabel.text = titles[index]
        return cell
    }
    
}


extension AppsVC: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return titles.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        
        
        switch index {
        case 0:

            return TopFreeController

        case 1:
            return  TopPaidController


            

        default: return UIViewController()
        }
        
    }
}


extension AppsVC: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
}


extension AppsVC: PagingContentViewControllerDelegate {
      func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
          menuViewController.scroll(index: index, percent: percent, animated: true)
      }
    
    
}
extension AppsVC : CustomNavigationController {
    func openRightMenu() {
        guard let menu = SideMenuVC().storyboardInstance() else {return}
        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu)
        SideMenuManager.default.rightMenuNavigationController = menuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        guard let rightMenu = SideMenuManager.default.rightMenuNavigationController   else {return}
         self.show(rightMenu, sender: nil)
    }
    
    func openLeftMenu() {
        guard let menu = SideMenuVC().storyboardInstance() else {return}
        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu)
        SideMenuManager.default.leftMenuNavigationController = menuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        guard let leftMenu = SideMenuManager.default.leftMenuNavigationController   else {return}
         self.show(leftMenu, sender: nil)
    }
    
    
  
    
    
}
