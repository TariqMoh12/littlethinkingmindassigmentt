//
//  SideMenuVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/24/21.
//

import UIKit
import SideMenu
import MOLH
import Firebase
class SideMenuVC : UITableViewController{
    
    var items = [NSLocalizedString("language", comment: ""), NSLocalizedString("favorite_folders", comment: ""), NSLocalizedString("logout", comment: "")]

     var nav: UINavigationController?
    
    func storyboardInstance() -> SideMenuVC? {
        let storyboard = UIStoryboard(name: "SideMenu", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "SideMenuVC") as? SideMenuVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.isScrollEnabled = false
        tableView.separatorStyle = .none
        tableView.layer.borderWidth = 1.0
        tableView.layer.borderColor = Constants.Colors.DarkGrayColor.cgColor
    }
   
   
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell" , for: indexPath)
        cell.textLabel?.textColor = .black
        cell.backgroundColor = .white
        cell.textLabel?.text = items[indexPath.row]
        if MOLHLanguage.isArabic(){
            cell.textLabel?.textAlignment = .left
        }else{
            cell.textLabel?.textAlignment = .right

        }
        cell.textLabel?.font = UIFont(name: "Arial", size: 19)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        switch indexPath.row {
        
        case 0 :
            UserDefaults.standard.set("1", forKey: "isLogin")
            MOLH.setLanguageTo(MOLHLanguage.currentAppleLanguage() == "en" ? "ar" : "en")
            MOLH.reset()
        case 1 :
            if let vc = FavoriteFoldersVC.storyboardInstance(){
                self.present(vc, animated: true, completion: nil)
            }
        case 2 :
            if !Connectivity.isConnectedToInternet {
                self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
               

            }else{
                do {
                    try Auth.auth().signOut()
                    self.dismiss(animated: true, completion: nil)
                    UserDefaults.standard.set("0", forKey: "isLogin")
                    let story = UIStoryboard(name: "Login", bundle: nil)
                    let viewController = story.instantiateViewController(withIdentifier: "rootnav")
                    UIApplication.shared.windows.first?.rootViewController = viewController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                    nav?.popToViewController(ofClass: LoginVC.self)
                   
                    } catch let err {
                        print(err)
                }
            }
       
        default:
            print("asdsa")
        }
    }
   

    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
         tableCellAnimation(cell: cell)
      }
}

