//
//  FavoritesDetailsVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/26/21.
//

import UIKit
import RealmSwift

class FavoritesDetailsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let realm = try! Realm()
//
    var folder :Results<Folders>? = nil
    var selectedndex:Int?
//    private let identifier = "AppsTableViewCell"
    
    

    
    static func storyboardInstance() -> FavoritesDetailsVC? {
        let storyboard = UIStoryboard(name: "Favorites", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "FavoritesDetailsVC") as? FavoritesDetailsVC
    } 

    
    private lazy var dataSource = FavoriteDetailsDataSource(self,tableView)
    private lazy var viewModel : FreeAppsViewModel = {
        let viewModel = FreeAppsViewModel(dataSource: dataSource)
        return viewModel
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.backgroundColor = .white
        folder =  realm.objects(Folders.self)
        dataSource.folder = folder
        dataSource.selectedndex = selectedndex
        tableView.reloadData()
    

        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
}
