//
//  FavoriteAppsVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/26/21.
//

import UIKit
import RealmSwift

class FavoriteAppsVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var allFolders:Array<Folders>!
    let realm = try! Realm()
    var model:Result?
    
    
    static func storyboardInstance() -> FavoriteAppsVC? {
        let storyboard = UIStoryboard(name: "Favorites", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "FavoriteAppsVC") as? FavoriteAppsVC
    }
    
    private lazy var dataSource = FavoriteListDataSourceTableView(self,tableView)
    private lazy var viewModel : FreeAppsViewModel = {
        let viewModel = FreeAppsViewModel(dataSource: dataSource)
        return viewModel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource.allFolders = allFolders
        self.dataSource.model = model
        self.tableView.reloadData()
    
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    


}
