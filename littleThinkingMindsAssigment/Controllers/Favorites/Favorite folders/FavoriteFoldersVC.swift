//
//  FavoriteFoldersVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import UIKit
import RealmSwift

class FavoriteFoldersVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var folder :Results<Folders>? = nil
    let realm = try! Realm()
//    private let identifier = "FavoritesTableViewCell"

    static func storyboardInstance() -> FavoriteFoldersVC? {
        let storyboard = UIStoryboard(name: "Favorites", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "FavoriteFoldersVC") as? FavoriteFoldersVC
    }
    
    private lazy var dataSource = FavoriteFoldersDataSourceTableView(self,tableView)
    private lazy var viewModel : FreeAppsViewModel = {
        let viewModel = FreeAppsViewModel(dataSource: dataSource)
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        folder =  realm.objects(Folders.self)
        
        self.dataSource.folder = folder
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name("ControllerDismissed"), object: nil)

    }
 

}

private extension FavoriteFoldersVC {
    
    func setup(){
        self.dataSource.data?.addAndNotify(observer: self) { [weak self] in
            guard let self = self else {return}
            self.tableView.reloadData()
        }
       
    }
    
}
