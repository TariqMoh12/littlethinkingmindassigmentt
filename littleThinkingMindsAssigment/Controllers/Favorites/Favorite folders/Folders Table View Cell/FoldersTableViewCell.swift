//
//  FoldersTableViewCell.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import UIKit

class FoldersTableViewCell: UITableViewCell {
    @IBOutlet weak var folderNameLabel: UILabel!
    @IBOutlet weak var foldersImageView: UIImageView!{
        didSet{
            foldersImageView.image = UIImage(named: "folder_ic")
        }
    }
    
    @IBOutlet weak var includedAppsCountLabel: UILabel!
    @IBOutlet weak var folderDescriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
