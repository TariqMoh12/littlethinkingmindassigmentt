//
//  CreateFolderVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/27/21.
//

import Foundation
import UIKit
import RealmSwift

class CreateFolderVC: UIViewController {
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var folderTitle: UITextField!
  
    @IBOutlet weak var folderDescriptionTextView: UITextView!
    
    let realm = try! Realm()

    lazy var CreateFolderVC : UIViewController = {
        let storyborad = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyborad.instantiateViewController(withIdentifier: "CreateFolderVC")
        return vc
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      
        setUpTextView()
        setUpViews()
    }
    
    private func setUpTextView(){
        folderDescriptionTextView.delegate = self
        folderDescriptionTextView.layer.borderWidth = 0.5
        folderDescriptionTextView.layer.borderColor = Constants.Colors.LightGrayColor.cgColor
        folderDescriptionTextView.textColor = Constants.Colors.LightGrayColor
        saveButton.layer.borderWidth = 1.0
        saveButton.layer.cornerRadius = 10.0
        saveButton.layer.borderColor = Constants.Colors.blackColor.cgColor
        saveButton.backgroundColor = Constants.Colors.mainBlue
        saveButton.setTitleColor(Constants.Colors.whiteColor, for: .normal)

    }
    
    private func setUpViews(){
        saveButton.setTitle(NSLocalizedString("save", comment: ""), for: .normal)
            descriptionLabel.text = NSLocalizedString("description", comment: "")
            titleLabel.text = NSLocalizedString("title", comment: "")
        folderTitle.text = ""
        folderTitle.textColor = Constants.Colors.DarkGrayColor
            folderDescriptionTextView.text = NSLocalizedString("description", comment: "")
        folderTitle.delegate = self
    }
    
    func checkID() -> String {
        let folders =  realm.objects(Folders.self)
        return String(folders.count + 1)
    }
    
    @IBAction func createTapped(_ sender: Any) {
        if folderTitle.text != "" && folderTitle.text != "Title" &&  folderTitle.text != "العنوان" && folderDescriptionTextView.text != "" && folderDescriptionTextView.text != "Description" && folderDescriptionTextView.text != "التفاصيل"{
            let folder = Folders()
                folder.folderName = folderTitle.text
                folder.folderDiscreptions = folderDescriptionTextView.text
                folder.folderID = checkID()
                realm.beginWrite()
                realm.add(folder)
                try! realm.commitWrite()
            self.presentAlert(withTitle: "", message: NSLocalizedString("adedd_sucessfully", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
        }else{
            self.presentAlert(withTitle: "", message: NSLocalizedString("fill_full_information", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
        }
    
    
    }
}
extension CreateFolderVC : UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == Constants.Colors.LightGrayColor {
              textView.text = nil
            textView.textColor = Constants.Colors.blackColor
          }
        
      }

      func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
             textView.text = NSLocalizedString("Desription", comment: "")
            textView.textColor = Constants.Colors.DarkGrayColor
         }
      }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n"  // Recognizes enter key in keyboard
            {
                textView.resignFirstResponder()
                return false
            }
            return true
        }
}

extension CreateFolderVC : UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
        textField.textColor = Constants.Colors.blackColor

    }
}
