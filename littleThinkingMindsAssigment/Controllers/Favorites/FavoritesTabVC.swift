//
//  FavoritesTabVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/26/21.
//

import UIKit
import PagingKit
import RealmSwift



class FavoritesTabVC: UIViewController {
    var menuViewController : PagingMenuViewController!
    var  contentViewController : PagingContentViewController!
    
    var titles = [String]()
    
    let realm = try! Realm()

    lazy var FavoritesController : UIViewController = {
        let storyborad = UIStoryboard(name: "Favorites", bundle: nil)
        let vc = storyborad.instantiateViewController(withIdentifier: "FavoritesTabVC")
        return vc
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        contentViewController.scrollView.isScrollEnabled = true
        contentViewController.view.backgroundColor = .clear
        setUpMenuViewController()
        self.title = NSLocalizedString("favorites", comment: "")
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("ControllerDismissed"), object: nil)
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if !titles.isEmpty {
            titles.removeAll()
        }
        let folders =  realm.objects(Folders.self)
        for item in folders {
            titles.append(item.folderName ?? "Fav")
        }
        self.contentViewController.reloadData()
        self.menuViewController.reloadData()
        self.title = NSLocalizedString("favorites", comment: "")

    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        if !titles.isEmpty {
            titles.removeAll()
        }
        let folders =  realm.objects(Folders.self)
        for item in folders {
            titles.append(item.folderName ?? "Fav")
        }
        self.contentViewController.reloadData()

        self.menuViewController.reloadData()
    }

    
    func setUpMenuViewController(){

        menuViewController.register(nib: UINib(nibName: "MenuCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
        menuViewController.registerFocusView(nib: UINib(nibName: "FocusView", bundle: nil))

        contentViewController.scroll(to: 0, animated: true) { (success) in
            self.contentViewController.reloadData()
            self.contentViewController.reloadData()

            self.menuViewController.reloadData()
        }
   
              

        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController{
            menuViewController = vc
            menuViewController.dataSource = self
            menuViewController.delegate = self

        }else if let vc = segue.destination as? PagingContentViewController{
            contentViewController = vc
                       contentViewController.dataSource = self
                       contentViewController.delegate = self

        }
    }



}
extension FavoritesTabVC: PagingMenuViewControllerDataSource {
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return titles.count
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return (UIScreen.main.bounds.width / CGFloat(titles.count))
    }
    
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: index) as! MenuCell
       // let cell = viewController.dequeueReusableCell(withReuseIdentifier: "TapView", for: index) as! TabView
        cell.titleLabel.text = titles[index]
        return cell
    }
    
}


extension FavoritesTabVC: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return titles.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        let storyborad = UIStoryboard(name: "Favorites", bundle: nil)
        let vc = storyborad.instantiateViewController(withIdentifier: "FavoritesDetailsVC") as! FavoritesDetailsVC
        vc.selectedndex = index
        
       return vc
        
    }
}


extension FavoritesTabVC: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
}


extension FavoritesTabVC: PagingContentViewControllerDelegate {
      func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
          menuViewController.scroll(index: index, percent: percent, animated: true)
      }
    
    
}
