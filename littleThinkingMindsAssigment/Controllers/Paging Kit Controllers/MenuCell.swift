//
//  TabView.swift
//  Athkar
//
//  Created by Tareq Mohammad on 8/15/20.
//  Copyright © 2020 Tareq Mohammad. All rights reserved.
//

import UIKit
import PagingKit

class MenuCell: PagingMenuViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var mainView: UIView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    override var isSelected: Bool{
        didSet{
            if isSelected {
                titleLabel.textColor = Constants.Colors.mainBlue
                self.mainView.layer.borderWidth = 1.0
                self.mainView.layer.borderColor = Constants.Colors.mainBlue.cgColor
            }else{
                titleLabel.textColor = .white
                self.mainView.layer.borderWidth = 0.0
                titleLabel.textColor = Constants.Colors.DarkGrayColor

            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
