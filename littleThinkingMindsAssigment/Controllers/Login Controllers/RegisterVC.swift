//
//  RegisterVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/27/21.
//

import UIKit
import Firebase
import MOLH
class RegisterVC: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    static func storyboardInstance() -> RegisterVC? {
        let storyboard = UIStoryboard(name: "Register", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        }
    
    private func setUpViews(){
        emailLabel.text = NSLocalizedString("email", comment: "")
        emailTextField.placeholder = NSLocalizedString("email", comment: "")
        passwordLabel.text = NSLocalizedString("password", comment: "")
        passwordTextField.placeholder = NSLocalizedString("password", comment: "")
        confirmPasswordLabel.text = NSLocalizedString("confirm_Password", comment: "")
        confirmPasswordTextField.placeholder = NSLocalizedString("confirm_Password", comment: "")
        registerButton.setTitle(NSLocalizedString("register", comment: ""), for: .normal)


        registerButton.layer.borderWidth = 1.0
        registerButton.layer.cornerRadius = 10.0
        registerButton.setTitleColor(Constants.Colors.whiteColor, for: .normal)
        registerButton.layer.borderColor = Constants.Colors.DarkGrayColor.cgColor
        registerButton.layer.backgroundColor = Constants.Colors.mainBlue.cgColor
        passwordTextField.isSecureTextEntry = true
        confirmPasswordTextField.isSecureTextEntry = true
        
           }
  

    @IBAction func registerTapped(_ sender: Any) {
        if !Connectivity.isConnectedToInternet {
            self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))

        }else{
            if emailTextField.text == "" || passwordTextField.text == "" || confirmPasswordTextField.text == ""{
                    self.presentAlert(withTitle: "" , message: NSLocalizedString("Enter_All_Credintials", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
                      }else{
                        
               if let password = passwordTextField.text, let confirmPassword = confirmPasswordTextField.text{
                    if password != confirmPassword{
                        self.presentAlert(withTitle: "" , message: NSLocalizedString("password_not_match", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
                        } else{
                        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { authResult, error in
                            if error != nil{
                                print(error?.localizedDescription as Any)
                                self.presentAlert(withTitle: "" , message: "\(error!.localizedDescription)", actionTitle: NSLocalizedString("ok", comment: ""))
                            }else{
                                if let loginVC = LoginVC.storyboardInstance(){
                                    self.navigationController?.pushViewController(loginVC, animated: true)
                                }
                            }
                        }
                    }
                    }
          
            }
        }
       
       
    }
}

