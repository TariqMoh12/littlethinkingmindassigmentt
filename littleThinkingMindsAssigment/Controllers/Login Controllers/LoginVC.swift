//
//  LoginVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/21/21.
//

import UIKit
import FirebaseDatabase
import Firebase
import MOLH
class LoginVC: UIViewController {

    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var notRegisteredLabel: UILabel!
    
    weak var vc : LoginVC?
    weak var homeVC : HomeTabBarVC?

    static func storyboardInstance() -> LoginVC? {
    let storyboard = UIStoryboard(name: "Login", bundle: nil)
    return storyboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginVC
        }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpViews()
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       }

    
    private func setUpViews(){
        
    loginButton.setTitle(NSLocalizedString("login", comment: ""), for: .normal)
    emailLabel.text = NSLocalizedString("email", comment: "")
    passwordLabel.text = NSLocalizedString("password", comment: "")
    notRegisteredLabel.text = NSLocalizedString("notRegistered", comment: "")
    registerButton.setTitle(NSLocalizedString("register", comment: ""), for: .normal)
        
        loginButton.layer.borderWidth = 1.0
        loginButton.layer.cornerRadius = 10.0
        loginButton.setTitleColor(Constants.Colors.whiteColor, for: .normal)
        loginButton.layer.borderColor = Constants.Colors.DarkGrayColor.cgColor
        loginButton.layer.backgroundColor = Constants.Colors.mainBlue.cgColor
        passwordTextField.isSecureTextEntry = true
       }
    
    @IBAction func loginTapped(_ sender: Any) {
        if !Connectivity.isConnectedToInternet {
            self.presentAlert(withTitle: "", message: NSLocalizedString("no_internet_connection", comment: ""), actionTitle: NSLocalizedString("ok", comment: ""))
        

        }else{
            if emailTextField.text == "" || passwordTextField.text == ""{
                if MOLHLanguage.isArabic(){
                    self.presentArabicAlert(withTitle: "!تنبيه", message: "البريد الإلكتروني او كلمة المرور لا يجب ان تكون فارغة")
                }else{
                    self.presentEnglishAlert(withTitle: "Invalid Login!", message: "Email or Password can not be empty")
                }
            }else{
                self.showSpinner(onView: self.view)

                Auth.auth().signIn(withEmail:emailTextField.text!, password: passwordTextField.text!) { (user, error) in

                         if error == nil{
                            self.removeSpinner()

                            if let HomeVC = HomeTabBarVC.storyboardInstance() {
                                self.navigationController?.pushViewController(HomeVC, animated: true)
                            }
                         }
                         else {
                            self.removeSpinner()

                            if MOLHLanguage.isArabic(){
                                self.presentArabicAlert(withTitle: "!تنبيه", message: "\(error!.localizedDescription)")
                            }else{
                                self.presentEnglishAlert(withTitle: "Invalid Login!", message: "\(error!.localizedDescription)")
                                }

                                   }
                               }
                              }
        }
       
                        }
    @IBAction func registerTapped(_ sender: Any) {
        if let registerVC = RegisterVC.storyboardInstance() {
            self.navigationController?.pushViewController(registerVC, animated: true)
        }
    }
}
