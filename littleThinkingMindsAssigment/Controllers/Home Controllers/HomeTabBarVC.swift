//
//  HomeTabBarVC.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/21/21.
//

import UIKit
import SideMenu
import MOLH
class HomeTabBarVC: UITabBarController {

    
    static func storyboardInstance() -> HomeTabBarVC? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: "HomeTabBarVC") as? HomeTabBarVC
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpTabBar()
        setupMenuNavigationItem()
        
       
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Apps", comment: "")

            }
    
    private func setUpTabBar(){
        self.tabBar.barTintColor = Constants.Colors.mainBlue
        self.tabBar.isTranslucent = false
        self.tabBar.unselectedItemTintColor = Constants.Colors.blackColor
        self.tabBar.tintColor = Constants.Colors.whiteColor
        self.navigationItem.hidesBackButton = true
    }

    private func setupMenuNavigationItem(){
        
       
        if MOLHLanguage.isArabic(){
            var item = UIBarButtonItem(image: UIImage(named: "menu_ic")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: .plain, target: self, action: #selector(self.openLeftMenu))
            item.tintColor = Constants.Colors.whiteColor
            navigationItem.rightBarButtonItem = item
        }else{
            var item = UIBarButtonItem(image: UIImage(named: "menu_ic")?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), style: .plain, target: self, action: #selector(self.openRightMenu))
            item.tintColor = Constants.Colors.whiteColor
            navigationItem.rightBarButtonItem = item
        }
        
    }

}


extension HomeTabBarVC : CustomNavigationController {
    func openRightMenu() {
        guard let menu = SideMenuVC().storyboardInstance() else {return}
        menu.nav = self.navigationController
        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu)
        SideMenuManager.default.rightMenuNavigationController = menuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        menuNavigationController.presentationStyle = .menuSlideIn
        
        guard let rightMenu = SideMenuManager.default.rightMenuNavigationController   else {return}
         self.show(rightMenu, sender: nil)
    }
    
    func openLeftMenu() {
        guard let menu = SideMenuVC().storyboardInstance() else {return}
        menu.nav = self.navigationController
        let menuNavigationController = UISideMenuNavigationController(rootViewController: menu)
        SideMenuManager.default.leftMenuNavigationController = menuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        menuNavigationController.presentationStyle = .menuSlideIn

        guard let leftMenu = SideMenuManager.default.leftMenuNavigationController   else {return}
         self.show(leftMenu, sender: nil)
    }
    
}
