//
//  FreeAppsModel.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import Foundation
import RealmSwift
// MARK: - FreeAppsModel
class FreeAppsModel: Codable {
    var feed: Feed?

    init(feed: Feed?) {
        self.feed = feed
    }
 
}

// MARK: - Feed
class Feed: Codable {
    var title: String?
    var id: String?
    var author: Author?
    var links: [Link]?
    var copyright, country: String?
    var icon: String?
    var updated: String?
    var results: [Result]?

    init(title: String?, id: String?, author: Author?, links: [Link]?, copyright: String?, country: String?, icon: String?, updated: String?, results: [Result]?) {
        self.title = title
        self.id = id
        self.author = author
        self.links = links
        self.copyright = copyright
        self.country = country
        self.icon = icon
        self.updated = updated
        self.results = results
    }
}

// MARK: - Author
class Author: Codable {
    var name: String?
    var uri: String?

    init(name: String?, uri: String?) {
        self.name = name
        self.uri = uri
    }
}

// MARK: - Link
class Link: Codable {
    var linkSelf: String?
    var alternate: String?

    enum CodingKeys: String, CodingKey {
        case linkSelf = "self"
        case alternate
    }

    init(linkSelf: String?, alternate: String?) {
        self.linkSelf = linkSelf
        self.alternate = alternate
    }
}

// MARK: - Result
class Result: Codable {
    var artistName, id, releaseDate, name: String?
    var kind: Kind?
    var copyright, artistID: String?
    var artistURL: String?
    var artworkUrl100: String?
    var genres: [Genre]?
    var url: String?
    var contentAdvisoryRating: String?
    var isFavorite = false

    enum CodingKeys: String, CodingKey {
        case artistName, id, releaseDate, name, kind, copyright
        case artistID = "artistId"
        case artistURL = "artistUrl"
        case artworkUrl100, genres, url, contentAdvisoryRating
    }

    init(artistName: String?, id: String?, releaseDate: String?, name: String?, kind: Kind?, copyright: String?, artistID: String?, artistURL: String?, artworkUrl100: String?, genres: [Genre]?, url: String?, contentAdvisoryRating: String?) {
        self.artistName = artistName
        self.id = id
        self.releaseDate = releaseDate
        self.name = name
        self.kind = kind
        self.copyright = copyright
        self.artistID = artistID
        self.artistURL = artistURL
        self.artworkUrl100 = artworkUrl100
        self.genres = genres
        self.url = url
        self.contentAdvisoryRating = contentAdvisoryRating
    }

}

// MARK: - Genre
class Genre: Codable {
    var genreID, name: String?
    var url: String?

    enum CodingKeys: String, CodingKey {
        case genreID = "genreId"
        case name, url
    }

    init(genreID: String?, name: String?, url: String?) {
        self.genreID = genreID
        self.name = name
        self.url = url
    }
}

enum Kind: String, Codable {
    case iosSoftware = "iosSoftware"
    case mobileSoftwareBundle = "mobileSoftwareBundle"

}
