//
//  NetworkManager.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import Foundation
import Moya

struct NetworkManager {
   
    static let shared =  NetworkManager()
    static let APIKey = ""
    let provider = MoyaProvider<Apis>(plugins: [NetworkLoggerPlugin()])
    
}
