//
//  Apis.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import Foundation
import Moya

enum Apis {
    case TopFreeApps(numberOfApps:Int)
    case TopPaidApps(numberOfApps:Int)

}

extension Apis: TargetType {
    var task: Task {
    
            return .requestPlain
    }
    
    var sampleData: Data {
        return stubbedResponse("Apps")
    }
    
    var baseURL: URL {
        switch self {
        case .TopFreeApps:
            guard let url = URL(string: Constants.Links.topFree) else { fatalError("baseURL could not be configured.")}
            return url
        case .TopPaidApps:
            guard let url = URL(string: Constants.Links.topPaid) else { fatalError("baseURL could not be configured.")}
            return url
    
            
        }
    }
    
    var path: String {
        switch self {
            
        case .TopFreeApps(let numberOfApps):
            return "\(numberOfApps)" + "/explicit.json"
            
        case .TopPaidApps(let numberOfApps):
            return "\(numberOfApps)" + "/explicit.json"

            
            
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    
    
    
    var validationType: ValidationType {
        return .successCodes
    }
    
    var headers: [String : String]? {
        switch self {
        case .TopPaidApps:
            return ["Accept": "application/json"]
            
        case .TopFreeApps:
            return ["Accept": "application/json"]
        }
        
    }
    
    
    func stubbedResponse(_ filename:String) -> Data! {
        @objc class TestClass : NSObject {}
        let bundel = Bundle(for: TestClass.self)
        let path = bundel.path(forResource: filename, ofType: "json")
        return (try? Data(contentsOf: URL(fileURLWithPath: path!)))
    }
    
    
    
    
    
    
    
}

