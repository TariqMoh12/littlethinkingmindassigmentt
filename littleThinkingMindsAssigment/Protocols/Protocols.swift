//
//  Protocols.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import Foundation
@objc protocol CustomNavigationController {
    @objc func openRightMenu()
    @objc func openLeftMenu()
}
