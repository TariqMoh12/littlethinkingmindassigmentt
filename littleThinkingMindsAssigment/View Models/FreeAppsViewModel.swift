//
//  FreeAppsViewModel.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/22/21.
//

import Foundation
import Moya

class FreeAppsViewModel {
    
    weak var dataSource : GenericDataSource<FreeAppsModel>?


    init(dataSource : GenericDataSource<FreeAppsModel>?) {
        self.dataSource = dataSource
    }
    
    var model:FreeAppsModel?
  
    
    func fetchData(numberOfApps : Int, completion: @escaping (Bool)->()){
        NetworkManager.shared.provider.request(.TopFreeApps(numberOfApps: numberOfApps)) { [weak self] result in
            print(Apis.TopFreeApps(numberOfApps: numberOfApps).baseURL)
            print(Apis.TopFreeApps(numberOfApps: numberOfApps).path)
            print(Apis.TopFreeApps(numberOfApps: numberOfApps).method)

        guard let self = self else { return }
            DispatchQueue.main.async {
                switch result {
                case .success(let response):
                  do {
                   print(response)
                    let dataM : FreeAppsModel = try JSONDecoder().decode(FreeAppsModel.self, from: response.data)

                    
                    if self.dataSource?.data?.value.isEmpty ?? true {
                        self.dataSource?.data?.value.append(dataM)
                    }else{
                        self.dataSource?.data?.value[0].feed?.results?.removeAll()
                        self.dataSource?.data?.value[0].feed?.results?.append(contentsOf: dataM.feed?.results ?? [])
                  }
                   completion(true)
                   
                  } catch {
                    print(error)
                   completion(false)
                  }
                case .failure(let error):
                    print(error.errorDescription)
                   completion(false)
                 
                }
            }
        }
    }
}
