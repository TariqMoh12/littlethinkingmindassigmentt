//
//  Connectivity.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/29/21.
//

import Foundation
struct Connectivity {
  static let sharedInstance = NetworkReachabilityManager()!
  static var isConnectedToInternet:Bool {
      return self.sharedInstance.isReachable
  }}
