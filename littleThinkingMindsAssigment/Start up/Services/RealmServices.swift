//
//  RealmServices.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/29/21.
//

import Foundation
import UIKit
import Realm
import RealmSwift
class RealmServices :NSObject {}

extension RealmServices : UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        let config = Realm.Configuration(
              schemaVersion: 1,
              migrationBlock: { migration, oldSchemaVersion in
                  if (oldSchemaVersion < 1) {
                  }
          })
          Realm.Configuration.defaultConfiguration = config
          let realm = try! Realm()
              
        
        return true
    }
}
