//
//  FirebaseServices.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/27/21.
//

import Foundation
import Firebase
import FirebaseCore

class FirebaseService :NSObject {}

extension FirebaseService : UIApplicationDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        FirebaseApp.configure()
        
        return true
    }
}
