//
//  SceneDelegate.swift
//  LangChnage
//
//  Created by Tareq Mohammad on 26/04/2021.
//

import UIKit
import MOLH

class SceneDelegate: UIResponder, UIWindowSceneDelegate,MOLHResetable {
    
    var window: UIWindow?
    private(set) static var shared: SceneDelegate?
    
    
    @available(iOS 13.0, *)
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        Self.shared = self
        guard let _ = (scene as? UIWindowScene) else { return }
        MOLHFont.shared.arabic = UIFont(name: "Courier", size: 13)!
        MOLHLanguage.setDefaultLanguage("en")
        MOLH.shared.activate(true)
        MOLH.shared.specialKeyWords = ["Cancel","Done"]
        reset()
        guard let _ = (scene as? UIWindowScene) else { return }
    }
    
    func reset() {
        if UserDefaults.standard.value(forKey: "isLogin") as! String == "0"{
            let rootViewController: UIWindow = (SceneDelegate.shared?.window)!
            let story = UIStoryboard(name: "Login", bundle: nil)
            rootViewController.rootViewController = story.instantiateViewController(withIdentifier: "rootnav")
        }else{
            let rootViewController: UIWindow = (SceneDelegate.shared?.window)!
            let story = UIStoryboard(name: "Main", bundle: nil)
            rootViewController.rootViewController = story.instantiateViewController(withIdentifier: "rootnav")
        }
        
    }
    
    @available(iOS 13.0, *)
    func sceneDidDisconnect(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    func sceneDidBecomeActive(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    func sceneWillResignActive(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    func sceneWillEnterForeground(_ scene: UIScene) {
        
    }
    
    @available(iOS 13.0, *)
    func sceneDidEnterBackground(_ scene: UIScene) {
        
    }
    
    
}

