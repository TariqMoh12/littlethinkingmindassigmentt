//
//  AppDelegate.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/20/21.
//

import UIKit
import Firebase
import MOLH
import Realm
import RealmSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let services: [UIApplicationDelegate] = [
        FirebaseService(),
        StyleService(),
        RealmServices()
           ]

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
     
        self.window?.rootViewController = UINavigationController(rootViewController:LoginVC())
        UserDefaults.standard.set("0", forKey: "isLogin")

        for service in self.services {
              service.application?(application, didFinishLaunchingWithOptions: launchOptions)
          }
        
       
        
        return true
    }

    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }



}

