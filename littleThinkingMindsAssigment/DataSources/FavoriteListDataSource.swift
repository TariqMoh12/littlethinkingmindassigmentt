//
//  FavoriteListDataSource.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import Foundation
import UIKit
import RealmSwift
class FavoriteListDataSourceTableView : GenericDataSource<FreeAppsModel>{
    var allFolders:Array<Folders>!
    let realm = try! Realm()
    var model:Result?

    weak var vc : FavoriteAppsVC?
    private let tableView: UITableView
    private let identifier = "FoldersTableViewCell"
    weak var viewModel : FreeAppsViewModel?

    
    init(_ vc : FavoriteAppsVC ,_ tableView: UITableView) {
         self.tableView = tableView
        self.vc = vc
         super.init()
         setup()
         registerCells()
        

     }
    
    func getTopViewController() -> UIViewController? {
        var topController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController
    }
    
    
  
  
}
extension FavoriteListDataSourceTableView : UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFolders.count
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // If it is loading section
       
        let cell = tableView.dequeueReusableCell(ofType: FoldersTableViewCell.self, for: indexPath)
        cell.folderNameLabel.text = allFolders?[indexPath.row].folderName
        cell.folderDescriptionLabel.text = allFolders?[indexPath.row].folderDiscreptions
        cell.includedAppsCountLabel.text = String(allFolders?[indexPath.row].appListName.count ?? 0)
        return cell
        
    }
   
  
    public func numberOfSections(in tableView: UITableView) -> Int {
        // Add one section for loader
        return 1
    }
    
  
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = allFolders?[indexPath.row]
        
       
        if object?.appListIds.contains(Int(model?.id ?? "") ?? 0) ?? false {
            let alert = UIAlertController(title: "", message: "Already in folder", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            
            let vc = getTopViewController()
            vc?.present(alert, animated: true, completion: nil)
            
        }else{
        try! realm.write {
            object?.appListIds.append((Int(model?.id ?? "") ?? 0))
            object?.appListName.append(model?.artistName ?? "")
            object?.appListURL.append(model?.artistURL ?? "")
            object?.appListImages.append(model?.artworkUrl100 ?? "")
            realm.add(object ?? Folders(), update: .modified)
            
            let vc = getTopViewController()

            let alert = UIAlertController(title: "", message: NSLocalizedString("adedd_sucessfully", comment: ""), preferredStyle: UIAlertController.Style.alert)
            let action = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                    // Completion block
                vc?.dismiss(animated: true, completion: nil)
            })
            alert.addAction(action)
            vc?.present(alert, animated: true, completion: nil)

        }
        }
    }
    
 
    

    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}


private extension FavoriteListDataSourceTableView {
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

    }
}


private extension FavoriteListDataSourceTableView {
    func registerCells(){
        //registerCells
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}


private extension FavoriteListDataSourceTableView {
    func removeCell(indexPath:IndexPath){
        //registerCells
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .fade)
        self.data?.value.remove(at: indexPath.row)
        self.tableView.endUpdates()
         
    }
}
