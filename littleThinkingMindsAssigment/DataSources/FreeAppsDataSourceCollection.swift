//
//  FreeAppsDataSourceCollection.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import Foundation
import UIKit
import RealmSwift
class FreeAppsDataSourceCollection : GenericDataSource<FreeAppsModel>{

    
    weak var vc : CollectionViewStyleViewController?
    private let collectionView: UICollectionView
    private let identifier = "FreeAppsCollectionViewCell"
    weak var viewModel : FreeAppsViewModel?
    let realm = try! Realm()

    var isDataLoading:Bool=false
    var pageNo:Int=0
    var limit:Int=20
    var offset:Int=0
    var didEndReached:Bool=false

    
    init(_ vc : CollectionViewStyleViewController ,_ collectionView: UICollectionView) {
         self.collectionView = collectionView
        self.vc = vc
         super.init()
         setup()
         registerCells()
     }
    
    @objc func favoriteTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let tappedImage = tapGestureRecognizer.view as! UIImageView
                print(tappedImage.tag)
        let index = IndexPath(row: tappedImage.tag, section: 0)

           let folders =  realm.objects(Folders.self)
           if folders.isEmpty {
               let alert = UIAlertController(title: "Alert", message: "Please creat folder", preferredStyle: UIAlertController.Style.alert)
               alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
               UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
           }else{
               var allFolders = Array<Folders>()
               for folder in folders {
                   allFolders.append(folder)

               }
               let storyborad = UIStoryboard(name: "Favorites", bundle: nil)
               let vc = storyborad.instantiateViewController(withIdentifier: "FavoriteAppsVC") as! FavoriteAppsVC
               vc.allFolders = allFolders
               vc.model = data?.value[0].feed?.results?[index.row]
               UIApplication.shared.keyWindow?.rootViewController?.present(vc, animated: true, completion: nil)

           }
           
               print("must set as favorite in realm")
   //        }else{
   //
   //        }
       }
    

}
extension FreeAppsDataSourceCollection : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (data?.value.isEmpty ?? true )  {
            return 0
        } else {
            return data?.value[0].feed?.results?.count ?? 0
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath as IndexPath) as! FreeAppsCollectionViewCell
        
               
        if data?.value.count != 0  {
            print(data?.value[0].feed?.results?[indexPath.row].artistName)
            cell.appNameLabel.text = data?.value[0].feed?.results?[indexPath.row].artistName
            cell.favoriteImageView.image = UIImage(named: "favorite_ic")
            cell.favoriteImageView.isUserInteractionEnabled = true
            cell.favoriteImageView.tag = indexPath.row
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(favoriteTapped(tapGestureRecognizer:)))
            cell.favoriteImageView.addGestureRecognizer(tapGestureRecognizer)
            if let imageURL = URL(string: data?.value[0].feed?.results?[indexPath.row].artworkUrl100 ?? "") {
                cell.appImageView.load(url: imageURL)
               
            }
           
        }
        
               
               return cell
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

            print("scrollViewWillBeginDragging")
            isDataLoading = false
        }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            print("scrollViewDidEndDecelerating")
        }
        //Pagination
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

                print("scrollViewDidEndDragging")
                if ((collectionView.contentOffset.y + collectionView.frame.size.height) >= collectionView.contentSize.height)
                {
                    
                    if !isDataLoading{
                        isDataLoading = true
                        self.pageNo=self.pageNo+10
                        self.limit=self.limit+10
                        self.offset=self.limit * self.pageNo
                        vc?.showSpinner(onView: vc?.view ?? UIView())
                        viewModel?.fetchData(numberOfApps: pageNo) { (bool) in
                            self.collectionView.reloadData()
                            self.vc?.removeSpinner()

                        }
                    }
                }


        }
    
    
}
extension FreeAppsDataSourceCollection: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 166, height: 140)
    }
    func collectionView(_ collectionView: UICollectionView,
                           layout collectionViewLayout: UICollectionViewLayout,
                           minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 5
       }

       func collectionView(_ collectionView: UICollectionView, layout
           collectionViewLayout: UICollectionViewLayout,
                           minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 5
       }
}


private extension FreeAppsDataSourceCollection {
    func setup() {
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout = UICollectionViewFlowLayout()
            layout.scrollDirection = .vertical
//        layout.itemSize = CGSize(width: 50, height: 50)
        collectionView.isPrefetchingEnabled = true
        collectionView.setCollectionViewLayout(layout, animated: false)
    }
}


private extension FreeAppsDataSourceCollection {
    func registerCells(){
        
        self.collectionView.register(UINib(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        
    }
}

