//
//  FavoriteFoldersDataSource.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import Foundation
import UIKit
import PaginatedTableView
import RealmSwift
class FavoriteFoldersDataSourceTableView : GenericDataSource<FreeAppsModel>{
    
    var folder :Results<Folders>? = nil
    let realm = try! Realm()
    
    weak var vc : FavoriteFoldersVC?
    private let tableView: UITableView
    private let identifier = "FoldersTableViewCell"
    weak var viewModel : FreeAppsViewModel?

    
    init(_ vc : FavoriteFoldersVC ,_ tableView: UITableView) {
         self.tableView = tableView
        self.vc = vc
         super.init()
         setup()
         registerCells()
        
     }
    
   
}
extension FavoriteFoldersDataSourceTableView : UITableViewDelegate, UITableViewDataSource {

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
       
      
            
 
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(folder?.count ?? 0)

        return folder?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(ofType: FoldersTableViewCell.self, for: indexPath)
        cell.selectionStyle = .none
        cell.folderNameLabel.text = folder?[indexPath.row].folderName
        cell.folderDescriptionLabel.text = folder?[indexPath.row].folderDiscreptions
        if let appsCount = folder?[indexPath.row].appListName.count{
            cell.includedAppsCountLabel.text = String(appsCount)

        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            // handle delete (by removing the data from your array and updating the tableview)
            let result = realm.objects(Folders.self)
                if result[indexPath.row].folderID == folder?[indexPath.row].folderID {
                    try! realm.write {
                    realm.delete(result[indexPath.row])
                    folder =  realm.objects(Folders.self)
                        tableView.reloadData()
                    }
                }

        }
    }

    
  
    
    
}


private extension FavoriteFoldersDataSourceTableView {
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

    }
}


private extension FavoriteFoldersDataSourceTableView {
    func registerCells(){
        //registerCells
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}


private extension FavoriteFoldersDataSourceTableView {
    func removeCell(indexPath:IndexPath){
        //registerCells
       
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .fade)
        self.data?.value.remove(at: indexPath.row)
        self.tableView.endUpdates()
         
    }
}
