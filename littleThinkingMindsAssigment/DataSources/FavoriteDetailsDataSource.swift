//
//  FavoriteDetailsDataSource.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/28/21.
//

import Foundation
import UIKit
import PaginatedTableView
import RealmSwift
class FavoriteDetailsDataSource : GenericDataSource<FreeAppsModel>{
    
    let realm = try! Realm()
  
    var folder :Results<Folders>? = nil
    var selectedndex:Int?
    private let identifier = "AppsTableViewCell"
        weak var vc : FavoritesDetailsVC?
    private let tableView: UITableView
    weak var viewModel : FreeAppsViewModel?

    
    init(_ vc : FavoritesDetailsVC ,_ tableView: UITableView) {
         self.tableView = tableView
        self.vc = vc
         super.init()
         setup()
         registerCells()
        
     }
    
    
    @objc func favoriteTapped(tapGestureRecognizer: UITapGestureRecognizer){
//        print(tapGestureRecognizer.tag)
        let tappedImage = tapGestureRecognizer.view as! UIImageView
                print(tappedImage.tag)
        let index = IndexPath(row: tappedImage.tag, section: 0)
        let object =  folder?[selectedndex ?? 0]
        try! realm.write {
            object?.appListIds.remove(at: index.row)
            object?.appListName.remove(at: index.row)
            object?.appListURL.remove(at: index.row)
            object?.appListImages.remove(at: index.row)
            realm.add(object ?? Folders(), update: .modified)
            folder =  realm.objects(Folders.self)
            self.tableView.reloadData()
        }
        
    }
    
   
}
extension FavoriteDetailsDataSource : UITableViewDelegate, UITableViewDataSource {

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
       
      
            
 
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if folder?[selectedndex ?? 0].appListName.count != 0 {
            return folder?[selectedndex ?? 0].appListName.count ?? 0

        }else{
            self.tableView.setEmptyMessage(NSLocalizedString("no_data", comment: ""))
            return 0
        }

        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(ofType: AppsTableViewCell.self, for: indexPath)
        cell.appNameLabel.text = folder?[selectedndex ?? 0].appListName[indexPath.row]
        if let imageURL = URL(string:folder?[selectedndex ?? 0].appListImages[indexPath.row] ?? "") {
            cell.appImageView.load(url: imageURL)

        }
        cell.favoriteImageView.image = UIImage(named: "favorite_ic_filled")
        cell.favoriteImageView.isUserInteractionEnabled = true
        cell.favoriteImageView.tag = indexPath.row
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(favoriteTapped(tapGestureRecognizer:)))
        cell.favoriteImageView.addGestureRecognizer(tapGestureRecognizer)

        
//        cell.navigationController = vc?.navigationController
//
        
        return cell
        
    }
    

    
  
    
    
}


private extension FavoriteDetailsDataSource {
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none

    }
}


private extension FavoriteDetailsDataSource {
    func registerCells(){
        //registerCells
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}


private extension FavoriteDetailsDataSource {
    func removeCell(indexPath:IndexPath){
        //registerCells
       
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .fade)
        self.data?.value.remove(at: indexPath.row)
        self.tableView.endUpdates()
         
    }
}
