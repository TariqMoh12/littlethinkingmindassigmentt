//
//  PaidAppsDataSource.swift
//  littleThinkingMindsAssigment
//
//  Created by Tareq Mohammad on 4/27/21.
//

import Foundation
import UIKit
import PaginatedTableView

class PaidAppsDataSource : GenericDataSource<FreeAppsModel>{

    weak var vc : TopPaidVC?
    private let tableView: UITableView
    private let identifier = "AppsTableViewCell"
    weak var viewModel : PaidAppsViewModel?
    var firtTime = 0
    
    var isDataLoading:Bool=false
    var pageNo:Int=0
    var limit:Int=20
    var offset:Int=0
    var didEndReached:Bool=false

    
    init(_ vc : TopPaidVC ,_ tableView: UITableView) {
         self.tableView = tableView
        self.vc = vc
         super.init()
         setup()
         registerCells()
     }
    
}
extension PaidAppsDataSource : UITableViewDelegate, UITableViewDataSource {
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 95
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let appUrlString = data?.value[0].feed?.results?[indexPath.row].artistURL{
            guard let appUrl = URL(string: appUrlString) else { return }
            UIApplication.shared.open(appUrl)

        }
 
    }
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (data?.value.isEmpty ?? true)  {
            
            return 0
        } else {
            return data?.value[0].feed?.results?.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(ofType: AppsTableViewCell.self, for: indexPath)
        cell.model = data?.value[0].feed?.results?[indexPath.row]
        cell.navigationController = vc?.navigationController
        return cell
        
    }

    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {

            print("scrollViewWillBeginDragging")
            isDataLoading = false
        }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
            print("scrollViewDidEndDecelerating")
        }
        //Pagination
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

                print("scrollViewDidEndDragging")
                if ((tableView.contentOffset.y + tableView.frame.size.height) >= tableView.contentSize.height)
                {
                    
                    if !isDataLoading{
                        isDataLoading = true
                        self.pageNo=self.pageNo+10
                        self.limit=self.limit+10
                        self.offset=self.limit * self.pageNo
                        vc?.showSpinner(onView: vc?.view ?? UIView())
                        viewModel?.fetchData(numberOfApps: pageNo) { (bool) in
                            self.tableView.reloadData()
                            self.vc?.removeSpinner()

                        }
                    }
                }


        }
    
    
}


private extension PaidAppsDataSource {
    func setup() {
        tableView.delegate = self
        tableView.dataSource = self
    }
}


private extension PaidAppsDataSource {
    func registerCells(){
        //registerCells
        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}


private extension PaidAppsDataSource {
    func removeCell(indexPath:IndexPath){
        self.tableView.beginUpdates()
        self.tableView.deleteRows(at: [indexPath], with: .fade)
        self.data?.value.remove(at: indexPath.row)
        self.tableView.endUpdates()
         
    }
}
